import React from 'react';
import logo from '../../logo.svg';
import {NavLink} from "react-router-dom";
import {logOut} from "../../actions/Authenticate";
import {connect} from "react-redux";
import {ICurrent} from "../../constant/types";

interface IProps {
    logOutConnect: () => void;
    isAuthenticated: boolean | null;
    uuid: string | null;
}

const Header = ({logOutConnect, isAuthenticated, uuid}: IProps) => {
    return (
        <header>
            <nav>
                <div className="nav-wrapper teal darken-3">
                    <NavLink to={"/"} className="brand-logo right">
                        <img src={logo} className="App-logo" alt="logo"/>
                    </NavLink>
                    <NavLink to={"/"} className="brand-logo right" style={{
                        marginRight: 100
                    }}>
                        <span>{uuid}</span>
                    </NavLink>
                    <ul id="nav-mobile" className="left hide-on-med-and-down">
                        <li><NavLink exact activeStyle={{backgroundColor: "rgba(0,0,0,0.1)"}} to={"/"}>Game</NavLink>
                        </li>
                        <li><NavLink exact activeStyle={{backgroundColor: "rgba(0,0,0,0.1)"}} to={"/user-board"}>User
                            Board</NavLink></li>

                        {isAuthenticated ? <NavLink to={"/"} className="waves-effect waves-light btn"
                                                    onClick={logOutConnect}>Logout</NavLink> :
                            <React.Fragment>
                                <li><NavLink exact activeStyle={{backgroundColor: "rgba(0,0,0,0.1)"}}
                                             to={"/login"}>Login</NavLink></li>
                                <li><NavLink exact activeStyle={{backgroundColor: "rgba(0,0,0,0.1)"}}
                                             to={"/register"}>Register</NavLink></li>
                            </React.Fragment>}

                    </ul>
                </div>
            </nav>
        </header>
    );
};
const mapDispatchToProps = {
    logOutConnect: logOut
};

const mapStateToProps = (state:{authenticate: ICurrent}) => ({
    uuid: state.authenticate.uuid,
    isAuthenticated: state.authenticate.isAuthenticated,
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Header);
