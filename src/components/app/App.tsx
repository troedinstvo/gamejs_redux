import React, {useEffect} from 'react';
import './App.scss';
import Header from "../header/header";
import {Switch, Route, Router} from "react-router-dom";
import LoggedInRoute from "../../routes/LoggedInRoute";
import GameBoard from "../../pages/gameBoard";
import UserBoard from "../../pages/userBoard";
import SignIn from "../../pages/signIn";
import SignUp from "../../pages/signUp";
import LoggedOutRoute from "../../routes/LoggedOutRoute";
import history from "../../history/history";
import {ICurrent} from "../../constant/types";
import {checkAuthentication} from "../../actions/Authenticate";
import {connect} from "react-redux";


interface IProps {
    checkAuthenticationConnect: () => void;
    isAuthenticated: boolean | null;
}

const App = (
    {
        checkAuthenticationConnect,
        isAuthenticated
    }: IProps) => {
    useEffect(() => {
        checkAuthenticationConnect()
    }, [checkAuthenticationConnect]);
    const app = isAuthenticated !== null ? (
        <Router history={history}>
            <Header/>
            <Route>
                <Switch>
                    <LoggedInRoute exact={true} path={"/"} component={GameBoard}/>
                    <LoggedInRoute exact={true} path={"/user-board"} component={UserBoard}/>
                    <LoggedOutRoute exact={true} path={"/login"} component={SignIn}/>
                    <LoggedOutRoute exact={true} path={"/register"} component={SignUp}/>
                    {/*<Route component={SignIn}/>*/}
                </Switch>
            </Route>
        </Router>
    ) : null;

    return (
        <div className="App">
            {app}
        </div>
    );
};

const mapStateToProps = (state: ICurrent) => ({
    isAuthenticated: state.isAuthenticated
});
const mapDispatchToProps = {
    checkAuthenticationConnect: checkAuthentication
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);
