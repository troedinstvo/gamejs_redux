import React from 'react';
import {connect} from "react-redux";
import {ICurrent, IGame} from "../../constant/types";

interface IProps {
    userRate?: []
}

const Rate= ({userRate}: IProps)=> {

    return (
        <div className={"rate-container"}>
            <h4>Rate</h4>
            <div className="row" style={{minHeight: 150}}>
                <React.Fragment>
                    {userRate?.map((elem: any, key: number)=>
                        <div className="col z-depth-1" key={elem.id}>
                            <img src={elem.images.downsized.url} alt={elem.title} width={"auto"} height={150} />
                        </div>
                    )}
                </React.Fragment>
            </div>
        </div>
    );
};

const mapStateToProps = (state: { game: IGame, authenticate: ICurrent }) => ({
    userRate: state.game.userRate
});

// const mapDispatchToProps = {
//
// };

export default connect(
    mapStateToProps,
    null
)(Rate)