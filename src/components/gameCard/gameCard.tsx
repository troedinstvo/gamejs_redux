import React, {useState} from 'react';
import {ICurrent, IGame, IShuffleArr, IUser} from "../../constant/types";
import {connect} from "react-redux";
import {Shuffle, Lose, Winner} from "../../actions/GameAction";

interface GameCardProps {
    shuffleArr: IShuffleArr[],
    ShuffleConnect: (arr: IShuffleArr[], index: number, status: boolean) => void,
    LoseConnect: (looseData: object[]) => void,
    WinnerConnect: (choose: object[]) => void,
    card: IShuffleArr,
    index: number,
    options: {
        empty: boolean,
        canClick: boolean,
        allActive: boolean,
    }
    userChoose?: [],
    userData?: {
        username: string,
        password: string,
        dataImage: []
    },
    allActive: () => void
}

export const GameCard = (
    {
        card,
        index,
        ShuffleConnect,
        shuffleArr,
        WinnerConnect,
        options,
        LoseConnect,
        userChoose,
        userData,
        allActive
    }: GameCardProps
) => {
    const [currentActive, setCurrentActive] = useState(card.activeStatus);
    return (
        <React.Fragment>
            <div className={currentActive?"col s2 active-item hoverable":
                options.allActive?"col s2 active-item hoverable":"col s2 hoverable"}
                 onClick={() => {
                     if (options.canClick) {
                         if (!card.status) {
                             console.log("click");
                             // @ts-ignore
                             LoseConnect(userData.dataImage);
                             allActive()
                         } else {
                             WinnerConnect(userChoose!);
                             setCurrentActive(true);
                             setTimeout(()=>{setCurrentActive(false)}, 900);
                         }
                         setTimeout(()=>{ShuffleConnect(shuffleArr, index, card.status)}, 1065);
                     }
                 }}
            >
                <p style={{
                    borderRadius: card.status ? 8 : 10,
                }}>{card.status ? "YOU WIN" : null}</p>
            </div>
        </React.Fragment>

    );
};
const mapDispatchToProps = {
    ShuffleConnect: Shuffle,
    LoseConnect: Lose,
    WinnerConnect: Winner
};

const mapStateToProps = (state: { game: IGame, authenticate: ICurrent }) => ({
    userChoose: state.game.userRate,
    userData: state.authenticate.user
});

export default connect(
    null,
    mapDispatchToProps
)(GameCard);