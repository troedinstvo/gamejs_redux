import React from 'react';
import {connect} from "react-redux";
import {ICurrent, IGame, IUser} from "../../constant/types";
import {UserRate} from "../../actions/GameAction";


interface IProps {
    userRateConnect: (choose: any)=> void,
    userData: IUser,
}

const WonPictures = ({userData, userRateConnect}: IProps) => {

    return (
        <>
            <h4>Won Pictures</h4>
            <div className="wrap" style={{minHeight: 150}}>
                <div className="row">
                    {userData.dataImage?.map((elem: any, key: number)=>(
                        <div className="col z-depth-1 hoverable" key={elem.id}
                        onClick={event => {
                            event.preventDefault();
                            userRateConnect(elem);
                        }}
                        >
                            <img src={elem.images.downsized.url} alt={elem.title} width={"auto"} height={150} />
                        </div>
                    ))}

                </div>
            </div>
        </>
    );
};

const mapStateToProps = (state: { game: IGame, authenticate: ICurrent }) => ({
    userData: state.authenticate.user
});
const mapDispatchToProps = {
    userRateConnect: UserRate
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(WonPictures);