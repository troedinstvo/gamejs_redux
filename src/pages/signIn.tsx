import React, {useState} from 'react';
import {logIn} from "../actions/Authenticate";
import {connect} from "react-redux";
import {IUser} from "../constant/types";


interface IProps {
    LogInConnect: (username: string, password: string) =>  void;
}

const SignIn = ({LogInConnect}: IProps) => {
    const [state, setState] = useState<IUser>({
        username: "",
        password: "",
        dataImage:[]
    });

    return (
        <div className="container">
            <div className="row">
                <form
                    className="col s6 offset-s3"
                    style={{paddingTop: 15}}
                >
                    <h5>Login</h5>
                    <div className="row">
                        <div className="input-field col s12">
                            <input
                                value={state.username}
                                id="last_name"
                                type="text"
                                className={"validate"}
                                // className={authStore.errors.length > 0? "validate invalid":"validate"}
                                onChange={(event => {
                                        event.preventDefault();
                                        setState({...state, username: event.target.value})
                                        // authStore.setUsername(event.target.value)
                                    }
                                )}
                            />
                            <label htmlFor="last_name">Name</label>
                            {/*<span className="helper-text" data-error={authStore.errors} data-success="right" />*/}
                        </div>
                        <div className="input-field col s12">
                            <input
                                id="password"
                                type="password"
                                className={"validate"}
                                // className={authStore.errors.length > 0? "validate invalid":"validate"}
                                value={state.password}
                                onChange={event => {
                                    event.preventDefault();
                                    setState({...state,
                                    password: event.target.value
                                    })
                                    // authStore.setPassword(event.target.value)
                                }}
                            />
                            <label htmlFor="password">Password</label>
                            {/*<span className="helper-text" data-error={authStore.errors} data-success="right" />*/}
                        </div>
                    </div>
                    <button className="waves-effect waves-light btn" onClick={(event)=>{
                        event.preventDefault();
                        LogInConnect(state.username,state.password);
                        // logIn("swat", "123")
                    }}>Login</button>
                </form>
            </div>
        </div>
    );
};

const mapDispatchToProps = {
    LogInConnect: logIn
};

export default connect(
    null,
    mapDispatchToProps
)(SignIn);