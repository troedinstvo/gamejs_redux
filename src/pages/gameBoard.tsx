import React, {useEffect, useState} from 'react';
import "../asset/gameCard.scss"
import GameCard from "../components/gameCard/gameCard";
import Rate from "../components/rate/rate";
import WonPictures from "../components/wonPictures/wonPictures";
import {ICurrent, IGame, IShuffleArr} from "../constant/types";
import {connect} from "react-redux";

interface IProps {
    shuffleArr: IShuffleArr[],
    user: {
        username: string,
        password: string,
        dataImage: []
    },
    userChoose: []
}

const GameBoard = ({shuffleArr, user, userChoose}: IProps) => {
    const [state, setState] = useState({
        options: {
            empty: false,
            canClick: true,
            allActive: false,
        },
        user: user
    });

    useEffect(() => {
        if (user.dataImage?.length > 0 && userChoose.length === 0) {
            setState({
                ...state,
                options: {
                    ...state.options,
                    canClick: false
                }
            })
        } else if (user.dataImage?.length > 0 && userChoose.length > 0) {
            setState({
                ...state,
                options: {
                    ...state.options,
                    canClick: true
                }
            })
        } else if (user.dataImage?.length === 0 && userChoose.length > 0) {
            setState({
                ...state,
                options: {
                    ...state.options,
                    canClick: true
                }
            })
        }
        // eslint-disable-next-line
    }, [user, userChoose]);

    function allActive() {
        setState({...state, options: {...state.options, allActive: true}});
        setTimeout(()=>{setState({...state, options: {...state.options, allActive: false}})}, 900);
    }

    return (
        <div>
            <h2>Game Board</h2>
            <div className="container game-container">
                <div className="row">
                    {shuffleArr.map((elem, key) =>
                        <GameCard
                            userData={user}
                            userChoose={userChoose}
                            options={state.options}
                            shuffleArr={shuffleArr}
                            card={elem}
                            key={key}
                            index={key}
                            allActive={allActive}
                        />
                    )}
                </div>
            </div>
            <div className="container-fluid">
                <div className="row">
                    <Rate/>
                </div>
                <div className="row">
                    <WonPictures/>
                </div>
            </div>
        </div>
    );
};
const mapStateToProps = (state: { game: IGame, authenticate: ICurrent }) => ({
    shuffleArr: state.game.shuffleArr,
    user: state.authenticate.user,
    userChoose: state.game.userRate
});

export default connect(
    mapStateToProps,
    null
)(GameBoard);