import React, {useState} from 'react';
import {connect} from "react-redux";
import {Register} from "../actions/Authenticate";
import {IUser} from "../constant/types";



interface IProps {
    RegisterConnect: (username: string, password: string) =>  void;
}

const SignUp = ({ RegisterConnect }: IProps) => {
    const [state, setState] = useState<IUser>({
        username: "",
        password: "",
        dataImage: []
    });
    return (
        <div className="container">
            <div className="row">
                <form
                    className="col s6 offset-s3"
                    style={{paddingTop: 15}}
                >
                    <h5>Register</h5>
                    <div className="row">
                        <div className="input-field col s12">
                            <input
                                id="last_name"
                                type="text"
                                className={"validate"}
                                // className={authStore.errors.length > 0? "validate invalid":"validate"}
                                value={state.username}
                                onChange={(event => {
                                    event.preventDefault();
                                    setState({...state, username: event.target.value})
                                    }
                                )}
                            />
                            <label htmlFor="last_name">Name</label>
                            {/*<span className="helper-text" data-error={authStore.errors} data-success="right" />*/}
                        </div>
                        <div className="input-field col s12">
                            <input
                                id="password"
                                type="password"
                                className={"validate"}
                                // className={authStore.errors.length > 0? "validate invalid":"validate"}
                                value={state.password}
                                onChange={event => {
                                    event.preventDefault();
                                    setState({...state, password: event.target.value})
                                    // authStore.setPassword(event.target.value)
                                }}
                            />
                            <label htmlFor="password">Password</label>
                            {/*<span className="helper-text" data-error={authStore.errors} data-success="right" />*/}
                        </div>
                    </div>
                    <button className="waves-effect waves-light btn" onClick={(event)=>{
                        event.preventDefault();
                        RegisterConnect(state.username, state.password)}
                    }>Register</button>
                </form>
            </div>
        </div>
    );
};

const mapDispatchToProps = {
    RegisterConnect: Register
};

export default connect(
    null,
    mapDispatchToProps,
) (SignUp);