import React from 'react';
import {connect} from "react-redux";
import {ICurrent, IUser} from "../constant/types";

interface IProps {
    userData: IUser
}

const UserBoard = ({userData}: IProps) => {
    return (
        <div>
            <h2>User board</h2>
            <div className="row">
                {userData.dataImage?.map((elem: any)=>(
                    <div className="col z-depth-1" key={elem.id}>
                        <img resource={"dsfsdf"} src={elem.images.downsized.url} alt={elem.title} width={"auto"} height={150} />
                    </div>
                ))}

            </div>
        </div>
    );
};

const mapStateToProps = (state: {authenticate: ICurrent}) => ({
    userData: state.authenticate.user
});

export default connect(
    mapStateToProps,
    null
)(UserBoard);