import {IGame} from "../constant/types";
import {IShuffle, IRate, ILoose, IWinner} from "../actions/GameAction";

export default function Game(
    state: IGame = {
        shuffleArr: [
            {
                status: true,
                activeStatus: false,
            },
            {
                status: false,
                activeStatus: false,
            },
            {
                status: false,
                activeStatus: false,
            }
        ],
        userRate: [],
        empty: true,
        canClick: true,
        allActive: false,
    },
    actions: IShuffle | IRate | ILoose | IWinner
) {
    switch (actions.type) {
        case "SHUFFLE":
            return {
                ...state,
                shuffleArr: actions.payload.newShuffle
            };
        case "RATE":
            return {
                ...state,
                userRate: state.userRate?.concat(actions.payload.choices)
            };
        case "LOOSE":
            return {
                ...state,
                userRate: []
            }
        case "WINNER":
            return {
                ...state,
                userRate: []
            }
    }
    return state
}