import {ICurrent} from "../constant/types";
import {IAuthenticate, IUnauthenticate} from "../actions/Authenticate";
import {IRate, IWinner} from "../actions/GameAction";

export default function Authenticate(
    state: ICurrent = {
        uuid: null,
        isAuthenticated: null,
        user: {
            username: '',
            password: '',
            dataImage: []
        }
    },
    action: IAuthenticate | IUnauthenticate | IRate | IWinner
): ICurrent {
    switch (action.type) {
        case "AUTHENTICATE":
            return {
                ...state,
                uuid: action.payload.username,
                isAuthenticated: true,
                user: {
                    username: action.payload.username,
                    password: action.payload.password,
                    dataImage: action.payload.dataImage
                }
            };
        case "UNAUTHENTICATE":
            return {
                uuid: null,
                isAuthenticated: false,
                user: {
                    username: '',
                    password: '',
                    dataImage: []
                }
            };
        case "RATE":
            return {
                ...state,
                user: {
                    ...state.user,
                    // @ts-ignore
                    dataImage: state.user.dataImage?.slice().filter((elem: any) =>elem.id !== action.payload.choices.id)
                }
            };
        case "WINNER":
            return {
                ...state,
                user:{
                    password: state.user.password,
                    username: state.user.username,
                    dataImage: state.user.dataImage.slice().concat(action.payload.image)
                }
            }
    }
    return state
}