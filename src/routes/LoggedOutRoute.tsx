import React, {ComponentType} from 'react';
import {Route} from "react-router-dom";
import { connect } from "react-redux";
import history from "../history/history";
import {ICurrent} from "../constant/types";




interface IProps {
    exact?: boolean;
    isAuthenticated: boolean | null
    path: string;
    component: ComponentType<any>
}
const LoggedOutRoute = ({component: Component,isAuthenticated, ...otherProps }: IProps) => {
    if (isAuthenticated) {
        history.push("/");
        // alert("this is a logged out route, you are logged in, redirected to home page");
    }
    return (
        <React.Fragment>
            <Route render={otherProps =>(
                <React.Fragment>
                    <Component {...otherProps}/>
                </React.Fragment>
            ) }/>
        </React.Fragment>
    );
};
const mapStateToProps = (state:{authenticate: ICurrent}) => ({
    isAuthenticated: state.authenticate.isAuthenticated
});

export default connect(
    mapStateToProps
)(LoggedOutRoute);