import {applyMiddleware, compose, createStore, combineReducers} from "redux";
import Authenticate from "../reducers/Authenticate";
import Game from "../reducers/Game";
import thunkMiddleware from "redux-thunk-recursion-detect";

let composeEnhancers;
if (
    process.env.NODE_ENV !== "production" &&
    (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
) {
    composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;
} else {
    composeEnhancers = compose;
}
const rootReducer = combineReducers({
    authenticate: Authenticate,
    game: Game
});

export const store = createStore<any, any, any, any>(
    rootReducer,
    undefined,
    composeEnhancers(applyMiddleware(thunkMiddleware)),
);

console.log(store.getState());