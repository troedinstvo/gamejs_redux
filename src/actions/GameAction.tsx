import {ThunkDispatch as Dispatch} from "redux-thunk";
import {LOOSE, RATE, SHUFFLE, WINNER} from "../constant/constants";
import {IShuffleArr, IUser} from "../constant/types";
import axios from "axios";


const giphy = {
    baseURL: "https://api.giphy.com/v1/gifs/",
    apiKey: "sXpGFDGZs0Dv1mmNFvYaGUvYwKX0PWIh",
    tag: "cats",
    type: "random",
    rating: "pg-13"
};

let giphyURL = encodeURI(
    giphy.baseURL +
    giphy.type +
    "?api_key=" +
    giphy.apiKey +
    "&tag=" +
    giphy.tag +
    "&rating=" +
    giphy.rating
);

export interface IShuffle {
    type: SHUFFLE,
    payload: any
}

function shuffle(newShuffle: IShuffleArr[]): IShuffle {
    return {
        type: SHUFFLE,
        payload: {
            newShuffle: newShuffle,
            empty: true,
            canClick: true,
            allActive: false
        }
    }
}

export interface IRate {
    type: RATE,
    payload: any
}

function userRate(choices: any) {
    return {
        type: RATE,
        payload: {
            choices: choices
        }
    }
}

export interface IWinner {
    type: WINNER,
    payload:{ image: object[]}
}

function winner(image: object[]) {
    return{
        type: WINNER,
        payload: {image: image}
    }
}


export function Winner(choose: any[]) {
    return async (dispatch: Dispatch<IWinner, {}, any>)=>{
        const auth_name = await window.localStorage["authenticated.name"];
        let user_data: IUser = await JSON.parse(window.localStorage[auth_name]);
        let image: object[] = [];
        // eslint-disable-next-line
        for (let key in choose) {
            image.push( await axios.get(giphyURL).then(function (response: any) {
                return response.data.data
            }))
        }
        if(choose.length === 0){
            image.push( await axios.get(giphyURL).then(function (response: any) {
                return response.data.data
            }))
        }
        console.log(choose.length);
        dispatch(winner(choose.concat(image)));
        console.log(image);
        let allData = user_data['dataImage'].concat(image);
        user_data['dataImage'] = allData;
        // console.log(user_data['dataImage']);
        window.localStorage[auth_name] = JSON.stringify(user_data);
        // console.log(choose);
    }

}

export interface ILoose {
    type: LOOSE
}

function loose() {
    return{
        type: LOOSE
    }
}

export function Lose(looseData: object[]) {
    return async (dispatch: Dispatch<ILoose, {}, any>)=>{
        const auth_name = await window.localStorage["authenticated.name"];
        let user_data: IUser = await JSON.parse(window.localStorage[auth_name]);
        user_data['dataImage'] = looseData;
        window.localStorage[auth_name] = JSON.stringify(user_data);
        dispatch(loose());
    }

}

export function UserRate(choices: any) {
    return async (dispatch: Dispatch<any, {}, any>) => {
        dispatch(userRate(choices))
    }
}

export function Shuffle(newShuffle: IShuffleArr[]) {
    return async (dispatch: Dispatch<IShuffleArr, {}, any>) => {

        const shuffleArr: IShuffleArr[] = await newShuffle.slice().sort(() => Math.random() - 0.5);
        dispatch(shuffle(shuffleArr))
    }

}

