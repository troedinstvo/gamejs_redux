import {ThunkDispatch as Dispatch} from "redux-thunk";

import {AUTHENTICATE, UNAUTHENTICATE} from "../constant/constants";
import {IUser} from "../constant/types";


export interface IAuthenticate {
    type: AUTHENTICATE;
    payload: IUser
}

function authenticate(username: string, password: string, dataImage: object[]): IAuthenticate {
    return {
        type: AUTHENTICATE,
        payload: {
            password,
            username,
            dataImage
        }

    };
}

export interface IUnauthenticate {
    type: UNAUTHENTICATE;
}

function unauthenticate(): IUnauthenticate {
    return {
        type: UNAUTHENTICATE,
    };
}

export type AuthenticationAction = IAuthenticate | IUnauthenticate;

export function logIn(username: string, password: string) {
    return async (dispatch: Dispatch<AuthenticationAction, {}, any>) => {
        const authNF = await window.localStorage[username];
        let auth: IUser = {password: "", username: "", dataImage: [{}]};
        if (!!authNF) {
            auth = JSON.parse(authNF)
        }
        if (auth.username === username && auth.password === password) {
            await window.localStorage.setItem("authenticated", "true");
            await window.localStorage.setItem("authenticated.name", username);

            dispatch(authenticate(username, password, auth.dataImage));
        }
    };
}

export function Register(username: string, password: string) {
    return async (dispatch: Dispatch<AuthenticationAction, {}, any>) => {
        const authNF = await window.localStorage[username];
        let auth: IUser = {username: username, password: password, dataImage: []};
        if (!!authNF) {
            auth = JSON.parse(authNF);
        }
        if (auth.username === username && !authNF) {
            await window.localStorage.setItem(username, JSON.stringify(auth));
            await window.localStorage.setItem("authenticated", "true");
            await window.localStorage.setItem("authenticated.name", username);

            dispatch(authenticate(username, password, []));
        }
    }
}

export function logOut() {
    return async (dispatch: Dispatch<AuthenticationAction, {}, any>) => {
        await window.localStorage.setItem("authenticated", "false");
        await window.localStorage.setItem("authenticated.name", "false");
        dispatch(unauthenticate());
    };
}

export function checkAuthentication() {
    return async (dispatch: Dispatch<AuthenticationAction, {}, any>) => {
        const auth = await window.localStorage.getItem("authenticated");
        const auth_name = await window.localStorage["authenticated.name"];
        const user_data = await JSON.parse(window.localStorage[auth_name]);
        // console.log("checkStatus");
        const formattedAuth = typeof auth === "string" ?
            JSON.parse(auth) :
            null;

        formattedAuth ? dispatch(authenticate(auth_name, user_data.password, user_data.dataImage)) : dispatch(unauthenticate());
    };
};