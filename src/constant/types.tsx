export interface ICurrent {
    isAuthenticated: boolean | null;
    uuid: string | null;
    user: IUser
}

export interface IUser {
    username: string,
    password: string,
    dataImage: object[]
}

export interface IGame {
    shuffleArr: IShuffleArr[],
    userRate?: any[],
    empty: boolean,
    canClick: boolean,
    allActive: boolean,
}

export interface IShuffleArr {
    status: boolean,
    activeStatus: boolean,
}