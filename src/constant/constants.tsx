
export const AUTHENTICATE = "AUTHENTICATE";
export type AUTHENTICATE = typeof AUTHENTICATE;

export const UNAUTHENTICATE = "UNAUTHENTICATE";
export type UNAUTHENTICATE = typeof UNAUTHENTICATE;

export const SHUFFLE = "SHUFFLE";
export type SHUFFLE = typeof SHUFFLE;

export const RATE = "RATE";
export type RATE = typeof RATE;

export const WINNER = "WINNER";
export type WINNER = typeof WINNER;

export const LOOSE = "LOOSE";
export type LOOSE = typeof LOOSE;